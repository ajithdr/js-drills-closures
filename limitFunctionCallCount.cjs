// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {

    if(arguments.length < 2) {
        throw new Error('pass callback and n value');
    }

    if(typeof cb !== 'function' || (typeof n !== 'number' && isNaN(n))) {
        throw new Error('invalid arguments');
    }
    
    function invokeCb(...args) {

        if(n > 0){
            n--;
            return cb(...args);
        } else {
            return null;
        }
    }

    return invokeCb;
}


module.exports = limitFunctionCallCount;

