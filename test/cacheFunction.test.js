const cacheFunction = require('../cacheFunction.cjs');

function add(...args) {
    sum = 0;
    for(let i = 0; i < args.length; i++) {
        sum += args[i];
    }
    return sum;
}

test('cache function', () => {
    expect(() => cacheFunction()).toThrow();
    expect(() => cacheFunction('cb')).toThrow();

    const res = cacheFunction(add);
    expect(res()).toBe(0);
    expect(res(1,2)).toBe(3);
    expect(res(1,2,3)).toBe(6);
    expect(res(1,2)).toBe(3);
})