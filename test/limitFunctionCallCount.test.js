const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

const sum = (...args) => {
    let sum = args.reduce((acc, cur) => {
        return acc + cur;
    }, 0);

    return sum;
}

test('call function only n times', () => {
    expect(() => limitFunctionCallCount()).toThrow();
    expect(() => limitFunctionCallCount('cb', 4)).toThrow();
    
    const res = limitFunctionCallCount(sum, 2);
    expect(res()).toBe(0);
    expect(res(1,2,3,4)).toBe(10);
    expect(res()).toBe(null);
})