const counterFactory = require('../counterFactory.cjs');


test('counter function', () => {
    
    const result = counterFactory();
    expect(result.increment()).toBe(1);
    expect(result.decrement()).toBe(0);
    expect(result.decrement()).toBe(-1);
    expect(result.increment()).toBe(0);
});