// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {

    if(arguments < 1 || typeof(cb) !== 'function') {
        throw new Error('invalid arguments');
    }
    
    let cache = {};
    function invokeCb(...args) {

        console.log(cache);
        if(args in cache) {
            return cache[args];
        } else {
            cache[args] = cb(...args);
            return cache[args];
        }
    }

    return invokeCb;
}

// function add(...args) {
//     sum = 0;
//     for(let i = 0; i < args.length; i++) {
//         sum += args[i];
//     }
//     return sum;
// }

// const func = cacheFunction(add);
// console.log(func(1,2));
// console.log(func(1,2));
// console.log(func(1,4));
// console.log(func(1,4,6));
// console.log(func(1,2));

module.exports = cacheFunction;


